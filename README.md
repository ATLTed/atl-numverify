# atlNumverify

Simple wrapper for numverify that uses async await.

See https://numverify.com


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-numverify.git#master
```

## Example Usage

```javascript
const atlNumverify = require('atl-numverify')('INSERT_KEY_HERE');

run();

async function run(){
  let r = await atlNumverify.validate('19028675309');
  console.log(r)
}
```