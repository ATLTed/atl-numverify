const axios = require('axios');
module.exports = function(key) {
  this.key = key;

  this.validate = async function(phone) {
    phone = phone.length === 10 ? `1${phone}` : phone;
    let url = `http://apilayer.net/api/validate?access_key=${this.key}&number=${phone}&format=1`
    let r = await axios.get(url)
    return r.data
  };
  return this;
};